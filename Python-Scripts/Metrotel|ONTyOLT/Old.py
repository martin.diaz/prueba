import time
import paramiko
import sys
import textfsm
import subprocess

param_interface = sys.argv[1]
param_display_ont = sys.argv[2]

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(hostname='192.168.137.59', username='grafana', password='',allow_agent=False,look_for_keys=False)

channel  = ssh.invoke_shell()

while not channel.recv_ready():
  print "Waiting for channel to be ready"
  time.sleep(1)

print "Executing..."

channel.send('enable\n')
time.sleep(1)

channel.send('config\n')
time.sleep(1)

channel.send('interface {}\n'.format(param_interface))
time.sleep(1)

channel.send('display ont {}\n'.format(param_display_ont))
time.sleep(1)

output = channel.recv(3000)

#for line in output.split('\n'):
#    print(line)

ssh.close()

print "---"

raw_text_data = output

#Comienza el parseo con TextFSM
template = open("template.txt")
re_table = textfsm.TextFSM(template)
fsm_results = re_table.ParseText(raw_text_data)
for row in fsm_results:
    print(row)

command = "curl -i -XPOST 'http://10.10.10.125:8086/write?db=cli_test' --data-binary 'optical_power,gpon_frame_id={},gpon_slot_id={},port_id={},ont_id={} rx={}'".format(fsm_results[0][0],fsm_results[0][1],fsm_results[0][2],fsm_results[0][3],100)
print command
subprocess.call(command,shell=True)
