import time
import paramiko
import sys
import textfsm
import mysql.connector

def connect_to(ip):
  try:
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname='{}'.format(ip), username='grafana', password='',allow_agent=False,look_for_keys=False, timeout = 4)
  except paramiko.SSHException as sshException:
    print("Unable to establish SSH connection: %s" % sshException)
  except (paramiko.SSHException, socket.error) as timeout:
    print("Err")
  return ssh


def run_remote_command(commands, ssh):
  channel = ssh.invoke_shell()
  while not channel.recv_ready():
    print "Waiting for channel to be ready"
    time.sleep(1)
  print "Ready. Now Working..."

  for cmd in commands:
     channel.send('{}\n'.format(cmd))
     time.sleep(1)

  output = channel.recv(4096)

  for line in output.split('\n'):
      print(line)

  return output

def run_local_command(commands):
    for command in commands:
        print command
        subprocess.call(command,shell=True)
        time.sleep(1)
    return

#Desc: raw_text_data es la informacion a parsear y template es el nombre del template usado para tal fin
def parse(raw_tex_data, template):#Comienza el parseo con TextFSM
    template = open("{}".format(template))
    re_table = textfsm.TextFSM(template)
    fsm_results = re_table.ParseText(raw_text_data)
    for row in fsm_results:
        print(row)
    return fsm_results

def get_equipoid_ip(ip,user,passwd,db):
    output = []
    conn = mysql.connector.connect(host='{}'.format(ip),user='{}'.format(user),passwd='{}'.format(passwd),db='{}'.format(db))
    mycursor = conn.cursor()
    mycursor.execute("SELECT equipos_id,id_equipo,marca,ip,estado,sync FROM equipos WHERE marca=23;")
    myresult = mycursor.fetchall()
    for x in myresult:
        output.append([x[0],x[3]])
    return output


out = get_equipoid_ip('127.0.0.1','root','','inventario_metrotel')

for tuple in out:
    print ("----------")
    print ("Connecting to: ",tuple[1])
    ssh = connect_to(tuple[1])
    print ("---------")
    print ("\n")
    ssh.close()

#cmd = ['enable','config','interface gpon 0/0','display ont optical info 0 0']
#ssh = connect_to('192.168.137.48')
#raw_text_data = get_run_remote_command(cmd,ssh)

#aca ejecuto la consulta al equipo

#raw_text_data = get_pwropt(sys.argv[1],sys.argv[2], ssh)

#ssh.close()

#fsm_results = parse(raw_text_data,"template.txt")

#cmd = ["curl -i -XPOST 'http://10.10.10.125:8086/write?db=cli_test' --data-binary 'optical_power,gpon_frame_id={},gpon_slot_id={},port_id={},ont_id={} rx={},tx={}'".format(fsm_results[0][0],fsm_results[0][1],fsm_results[0][2],fsm_results[0][3],fsm_results[0][4],fsm_results[0][5])]
#run_local_command(cmd)
