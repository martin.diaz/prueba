Value ARQUITECTURA ([^\\]+)
Value CPUS ([^\\]+)
Value NAME ([^\\]+)

Start
  ^Architecture:\s+${ARQUITECTURA}
  ^CPU\(s\):\s+${CPUS}
  ^Model name:\s+${NAME} -> Record
